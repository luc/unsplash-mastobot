# Unsplash mastobot

![Unsplash mastobot logo](https://framagit.org/uploads/-/system/project/avatar/21085/camera-photo.png)

This software is a [Mastodon](https://joinmastodon.org) bot that posts a random photo from [Unsplash](https://unsplash.com).

## How to use?

First, get the software:

```
sudo apt install git
git clone https://framagit.org/luc/unsplash-mastobot
cd unsplash-mastobot
```

Then register your app, considering that your Mastodon instance is `exemple.org`:

```
sudo cpan Carton
carton install
HOST="exemple.org" carton exec ./register-app.pl
```

This will give you three secret environment variables: `CLIENT_ID`, `CLIENT_SECRET` and `ACCESS_TOKEN`.
Keep them well and secure!

Then:
- if you want to run the bot from your computer, you'll need to install a few things:

```
sudo apt install perlmagick libnet-ssleay-perl libio-socket-ssl-perl build-essential
sudo cpan Mojolicious Image::Randim Mastodon::Client
```

  Then just set your three secret environment variables and the `HOST` like before and do `./unsplash-mastobot.pl`;

- if you want to run the bot on a Gitlab instance with enabled CI, set `HOST`, `CLIENT_ID`, `CLIENT_SECRET` and `ACCESS_TOKEN` in your repository "CI/CD > Secret variables" settings and trigger a pipeline.
It will use a custom Docker image to execute the software.

## License

GPLv3, see the [LICENSE](LICENSE) file for details.

## Logo

The logo is « [tango camera photo](https://openclipart.org/detail/34423/tango-camera-photo) » by [warszawianka](https://openclipart.org/user-detail/warszawianka), public domain.

## Author

[Luc Didry](https://fiat-tux.fr). You can support me on [Tipeee](https://tipeee.com/fiat-tux) and [Liberapay](https://liberapay.com/sky).

![Tipeee button](themes/default/img/tipeee-tip-btn.png) ![Liberapay logo](themes/default/img/liberapay.png)
